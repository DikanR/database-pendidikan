<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class Pelajar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('database_pelajar', function (Blueprint $table) {
            $table->id();
            // $table->unsignedBigInteger('id');
            // $table->foreignId('user_id')->references('id')->on('database_pelajar')
            $table->string('nik');
            $table->string('nama');
            $table->string('tempat_lahir');
            $table->string('tanggal_lahir');
            $table->string('agama');
            $table->string('kelamin');
            $table->string('orang_tua');
            $table->string('sekolah_nama');
            // $table->foreignId('sekolah_id');
            // $table->string('kota_nama');
            $table->foreignId('kotamodel_id');
            // $table->string('provinsi_nama');
            $table->foreignId('provinsimodel_id');
            $table->string('status_sekolah');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('database_pelajar');
    }
}
