<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kotamodels', function (Blueprint $table) {
            $table->id();
            $table->string('kota');
            $table->string('slug_kota')->unique();
            $table->timestamps();
        });

        DB::table('kotamodels')->insert([
            //Kalimantan Barat
            ['id' => 1, 'kota' => 'Pontianak', 'slug_kota' => 'pontianak'],
            //Kalimantan Selatan
            ['id' => 2, 'kota' => 'Banjarmasin', 'slug_kota' => 'banjarmasin'],
            //Kalimantan Tengah
            ['id' => 3, 'kota' => 'Palangkaraya', 'slug_kota' => 'palangkaraya'],
            //Kalimantan Timur
            ['id' => 4, 'kota' => 'Samarinda', 'slug_kota' => 'samarinda'],
            //Kalimantan Utara
            ['id' => 5, 'kota' => 'Tanjung Selor', 'slug_kota' => 'tanjung_selor'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kotamodels');
    }
};
