<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class Kota extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('database_kota', function (Blueprint $table) {
            $table->id();
            $table->string('kota');
            $table->string('slug_kota')->unique();
            $table->foreignId('provinsi_id');
            $table->timestamps();
        });

        DB::table('database_kota')->insert([
            //Kalimantan Barat
            ['id' => 1, 'kota' => 'Pontianak', 'slug_kota' => 'pontianak', 'provinsi_id' => 1],
            //Kalimantan Selatan
            ['id' => 2, 'kota' => 'Banjarmasin', 'slug_kota' => 'banjarmasin', 'provinsi_id' => 2],
            //Kalimantan Tengah
            ['id' => 3, 'kota' => 'Palangkaraya', 'slug_kota' => 'palangkaraya', 'provinsi_id' => 3],
            //Kalimantan Timur
            ['id' => 4, 'kota' => 'Samarinda', 'slug_kota' => 'samarinda', 'provinsi_id' => 4],
            //Kalimantan Utara
            ['id' => 5, 'kota' => 'Tanjung Selor', 'slug_kota' => 'tanjung_selor', 'provinsi_id' => 5],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('database_kota');
    }
}
