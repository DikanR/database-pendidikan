<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provinsimodels', function (Blueprint $table) {
            $table->id();
            $table->string('provinsi');
            $table->string('slug_provinsi')->unique();
            $table->timestamps();
        });

        DB::table('provinsimodels')->insert([
            //Kalimantan
            ['id' => 1, 'provinsi' => 'Kalimantan Barat', 'slug_provinsi' => 'kalimantan-barat'],
            ['id' => 2, 'provinsi' => 'Kalimantan Selatan', 'slug_provinsi' => 'kalimantan-selatan'],
            ['id' => 3, 'provinsi' => 'Kalimantan Tengah', 'slug_provinsi' => 'kalimantan-tengah'],
            ['id' => 4, 'provinsi' => 'Kalimantan Timur', 'slug_provinsi' => 'kalimantan-timur'],
            ['id' => 5, 'provinsi' => 'Kalimantan Utara', 'slug_provinsi' => 'kalimantan-utara'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provinsimodels');
    }
};
