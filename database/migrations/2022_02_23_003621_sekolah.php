<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class Sekolah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('database_sekolah', function (Blueprint $table) {
            $table->id();
            $table->string('npsn');
            $table->string('nama_sekolah');
            $table->string('alamat_sekolah');
            $table->string('akreditasi_sekolah');
            $table->foreignId('provinsi_id');
            $table->foreignId('kota_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('database_sekolah');
    }
}
