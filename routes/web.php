<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\database_controller;
use App\Http\Controllers\auth_controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/sekolah/export/{id}', [database_controller::class, 'PDFpelajar']);
//Input
Route::get('inputpelajar', [database_controller::class, 'create']);
Route::post('inputpelajar', [database_controller::class, 'store']);
Route::get('/destroy/{id}', [database_controller::class, 'destroy']);
Route::get('/edit/{id}', [database_controller::class, 'edit']);
Route::post('/edit/{id}/submit', [database_controller::class, 'editsubmit']);

Route::get('/cari', [database_controller::class, 'cari']);
//PDF

Route::get('/lokasi/{Provinsi}/pdf-provinsi', [database_controller::class, 'PDFprovinsi']);
Route::get('/lokasi/{Provinsi}/{Kota}/pdf-kota', [database_controller::class, 'PDFkota']);
Route::get('/lokasi/{Provinsi}/{Kota}/{NPSN}/pdf-sekolah', [database_controller::class, 'PDFsekolah']);

Route::get('/{Provinsi}/pdf-provinsi', [database_controller::class, 'PDFprovinsi']);
Route::get('/{Provinsi}/{Kota}/pdf-kota', [database_controller::class, 'PDFkota']);
Route::get('/{Provinsi}/{Kota}/{NPSN}/pdf-sekolah', [database_controller::class, 'PDFsekolah']);

//Halaman
Route::group(['middleware' => 'auth'], function () {
    Route::get('/index', [database_controller::class, 'pelajar']);
});
Route::get('/lokasi/{Provinsi}', [database_controller::class, 'provinsi']);
Route::get('/lokasi/{Provinsi}/{Kota}', [database_controller::class, 'kota']);
Route::get('/lokasi/{Provinsi}/{Kota}/{NPSN}', [database_controller::class, 'sekolah']);

//Login
Route::get('/', [auth_controller::class, 'login'])->name('login');
Route::post('/login/submit', [auth_controller::class, 'loginsubmit']);

//registration
Route::get('/registration', [auth_controller::class, 'registration']);
Route::post('/registration/submit', [auth_controller::class, 'registrationsubmit']);


Route::get('/logout', [auth_controller::class, 'logout']);
