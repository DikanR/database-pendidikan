<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="Naufal Elghani, Mark Otto, Jacob Thornton, and Bootstrap contributors">
  <meta name="generator" content="Hugo 0.88.1">

  <!-- Bootstrap core CSS -->
  <!-- <link href="assets/dist/css/bootstrap.min.css" rel="stylesheet"> -->

  <!-- CSS only -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <title>Database Pendidikan</title>

  <style>
    .bd-placeholder-img {
      font-size: 1.125rem;
      text-anchor: middle;
      -webkit-user-select: none;
      -moz-user-select: none;
      user-select: none;
    }

    @media (min-width: 768px) {
      .bd-placeholder-img-lg {
        font-size: 3.5rem;
      }
    }
  </style>


  <!-- Custom styles for this template -->
  <link href="dashboard.css" rel="stylesheet">
</head>

<body>

  <!-- NAVBAR YANG DITENGAH -->
  <header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3 text-center" href="#">Database Pendidikan</a>
    <div class="container row navbar-nav nav-item">
      <ul class="nav me-auto justify-content-md-center px-3">
        @foreach ($titles as $tt)
        <li class="nav-item"><a href="{{ $navlink[$tt] }}" class="nav-link link-dark px-2 {{ ($active === $navlink[$tt]) ? 'active' : '' }}">{{ $tt }}</a></li>
        <!-- <li class="nav-item"><a href="#" class="nav-link link-dark px-2 active">Data Siswa</a></li>
        <li class="nav-item"><a href="#" class="nav-link link-dark px-2">Intergrasi</a></li> -->
        @endforeach
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
            Lainnya
          </a>
          <ul class="dropdown-menu position-absolute" aria-labelledby="dropdownMenuLink">
            <li><a class="dropdown-item" href="#">Informasi</a></li>
            <li><a href="{{ url('/logout') }}" class="dropdown-item">Logout</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </header>

  <div class="container-fluid">
    <div class="row">
      <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
        <div class="position-sticky pt-3">
          <ul class="nav flex-column">
            @foreach ($titles as $tx)
            <li class="nav-item">
              <a class="nav-link {{ ($active === $navlink[$tx]) ? 'active' : '' }}" href="{{ $navlink[$tx] }}">
                <span data-feather="{{ $icon[$tx] }}"></span>
                {{ $tx }}
              </a>
            </li>
            @endforeach
          </ul>

          <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>Lainnya</span>


            <!-- BUAT NAMBAH LIST KALI AJA ADA YANG MAU DITAMBAH -->
            <!-- <a class="link-secondary" href="#" aria-label="Add a new report">
              <span data-feather="plus-circle"></span>
            </a> -->

          </h6>
          <ul class="nav flex-column mb-2">
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="file"></span>
                Informasi
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/logout') }}">
                <span data-feather="log-out"></span>
                Logout
              </a>
            </li>
          </ul>
        </div>
      </nav>

      <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
        @yield('body')
</body>

</html>