<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="login.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title>Document</title>
</head>

<body>
    <div class="header container-fluid border-bottom border-dark">
        <div class="row">
            <div class="col-sm">
                <div class="navbar justify-content-center">
                    <img class="navbar-brand img-fluid" src="login.png" alt="" width="50px" height="" style="position: relative; right: 20px;">
                    <p class="text-center h1 " style="position: relative; right: 50px;">Database Pendidikan Kota Banjarmasin</p>
                </div>
                <div class="">
                    <p class="text-center fs-6" style="position: relative; left: 25px; bottom: 60px;">Jln.Brigjend Hasan Basri No 6,Sungai Miai,Kec Banjarmasin Utara,Kota Banjarmasin</p>
                </div>
            </div>
        </div>

    </div>
    <div id="login-form">
        <form class="login" method="post" action="/registration/submit">
            @csrf
            <input type="text" name="username" class="@error('username') is-invalid @enderror" placeholder="Username" style="height:40px; width: 350px; font-style:italic ;border-radius: 9px; text-align: center;" required>
            @error('username')
            <div class="invalid-feedback">
                Minimal Username adalah 3 Karakter.
            </div>
            @enderror
            <br>
            <br>
            <input name="password" class="bi bi-person-fill @error('password') is-invalid @enderror" type="Password" style="height:40px; width: 350px; font-style:italic ;border-radius: 9px; text-align: center;" placeholder="Masukkan Password" required>
            @error('password')
            <div class="invalid-feedback">
                Minimal Password adalah 8 Karakter.
            </div>
            @enderror
            <br>
            <br>
            <div class="col-auto">
                <button type="submit" class="btn" style="width: 140px; background-color: #5D5ACB; color: white; border-radius: 25px;">Register</button>
            </div>
        </form>
    </div>
    <!--Footer-->
    <div class=" text-light py-3 fixed-bottom" style="background-color: #5d5acb;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm text-center">
                    <div>
                        <a class="fs-6 text-decoration-none text-light">Copyright&copy;2022 SMKN 2 Banjarmasin</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


</body>

</html>