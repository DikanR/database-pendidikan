@extends('template.hometemplate')

@section('body')
<div class="container-fluid border-bottom border-dark" style="height: 180px; background-color: #5d5acb; color: azure">
    <div class="navbar justify-content-center">
        <!-- <img class="navbar-brand img-fluid" src="../img/tut_wuri.png" alt="" width="140" height="100" style="position: relative; right: 60px" />
    <p class="text-center h1" style="position: relative; right: 50px">
      Database Pendidikan Kota Banjarmasin
    </p>
    <p class="text-center h6" style="position: relative; left: 20px; bottom: 35px">
      Jln.Brigjend Hasan Basri No 6,Sungai Miai,Kec Banjarmasin
      Utara,Kota Banjarmasin
    </p> -->
        <div class="header">
            <a href="#default" class="logo">Database Pendidikan Kota Banjarmasin</a>
        </div>
    </div>
</div>

<div class="container">
    <div class="card" style="margin-bottom: 10%; margin-top: 5%;">
        <div class="card-header bg-success text-white" style="text-align: center;">Input Data Siswa</div>
        <div class="card-body">
            <form action="/edit/{{ $pelajar->id }}/submit" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group" style="width: 50%;">
                    <label for="nama">NIK</label>
                    <input type="text" class="form-control" name="nik" value="{{ $pelajar->nik }}">
                </div>
                <div class="form-group" style="width: 50%;">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" name="nama" value="{{ $pelajar->nama }}">
                </div>

                <div class="form-group" style="width: 50%;">
                    <label for="tempat_lahir">Tempat/Tanggal Lahir</label>
                    <input type="text" class="form-control" name="tempat_lahir" value="{{ $pelajar->tempat_lahir }}">
                    <input type="date" class="form-control" name="ttl">
                </div>



                <div class="form-group" style="width: 50%;">
                    <label for="agama">Agama</label>
                    <select class="form-control" name="agama">
                        <option>Islam</option>
                        <option>Protestan</option>
                        <option>Buddha</option>
                        <option>Hindu</option>
                        <option>Katolik</option>
                        <option>Khonghucu</option>
                    </select>
                </div>

                <div class="form-group" style="width: 50%;">
                    <label for="kelamin">Kelamin</label>
                    <select class="form-control" name="kelamin">
                        <option>Laki-Laki</option>
                        <option>Perempuan</option>
                    </select>
                </div>
                <div class="form-group" style="width: 50%;">
                    <label for="orang_tua">Orang Tua</label>
                    <input type="text" class="form-control" name="orang_tua" value="{{ $pelajar->orang_tua }}">
                </div>
                <div class="form-group" style="width: 50%;">
                    <label for="sekolah_nama">Nama Sekolah</label>
                    <input type="text" class="form-control" name="sekolah_nama" value="{{ $pelajar->sekolah_nama }}">
                </div>
                <div class="form-group" style="width: 50%;">
                    <label for="status_sekolah">Status Siswa</label>
                    <select class="form-control" name="status_sekolah">
                        <option>Tidak Lulus</option>
                        <option>Lulus</option>
                    </select>
                </div>

                <div class="form-group" style="width: 50%;">
                    <label for="provinsi">Provinsi</label>
                    <select class="form-control" name="provinsi">
                        <option>Kalimantan Barat</option>
                        <option>Kalimantan Selatan</option>
                        <option>Kalimantan Tengah</option>
                        <option>Kalimantan Timur</option>
                        <option>Kalimantan Utara</option>
                    </select>
                </div>

                <div class="form-group" style="width: 50%;">
                    <label for="kota">Kota</label>
                    <select class="form-control" name="kota">
                        <option>Pontianak</option>
                        <option>Banjarmasin</option>
                        <option>Palangkaraya</option>
                        <option>Samarinda</option>
                        <option>Tanjung Selor</option>
                    </select>
                </div>

                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
            </form>

            <div class="text-light py-3 fixed-bottom bg-dark" style="">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm text-center">
                            <div>
                                <a class="fs-6 text-decoration-none text-light">Copyright&copy;2022 SMKN 2 Banjarmasin</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <script src="assets/dist/js/bootstrap.bundle.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js" integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js" integrity="sha384-zNy6FEbO50N+Cg5wap8IKA4M/ZnLJgzc6w2NqACZaK0u0FXfOWRRJOnQtpZun8ha" crossorigin="anonymous"></script>
<script src="dashboard.js"></script>
<style>
    /* Style the header with a grey background and some padding */
    .header {

        overflow: hidden;
        background-color: #5d5acb;
        padding: 20px 10px;
    }

    /* Style the header links */
    .header a {
        float: left;
        color: white;
        text-align: center;
        padding: 12px;
        text-decoration: none;
        font-size: 18px;
        line-height: 25px;
        border-radius: 4px;
        font-style: normal;
    }

    /* Style the logo link (notice that we set the same value of line-height and font-size to prevent the header to increase when the font gets bigger */
    .header a.logo {
        font-size: 25px;
        font-weight: bold;
    }

    /* Change the background color on mouse-over */
    .header a:hover {
        background-color: #ddd;
        color: black;
    }

    /* Style the active/current link*/
    .header a.active {
        background-color: dodgerblue;
        color: white;
    }

    /* Add media queries for responsiveness - when the screen is 500px wide or less, stack the links on top of each other */
    @media screen and (max-width: 1200px) {
        .header a {
            float: none;
            display: block;
            text-align: center;
        }
    }
</style>

@endsection