@extends('template.hometemplate')

@section('body')
<!-- <h2>Data Siswa</h2> -->

<!-- TOMBOL SEARCH GOIB -->
<form action="/cari" method="GET">
  <input class="form-control form-control-dark w-100" name="cari" type="text" placeholder="Search..." aria-label="Search">
  <button class="navbar-toggler position-absolute d-md-none collapsed" type="submit" value="Cari" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
</form>

<div class="table-responsive">
  <table class="table table-striped table-sm">
    <thead>
      <tr>
        <th rowspan="2">No</th>
        <th rowspan="2">NPSN</th>
        <th rowspan="2">Nama</th>
        <th rowspan="2">Tempat tanggal lahir</th>
        <th rowspan="2">Kelamin</th>
        <th rowspan="2">Nama Orang Tua</th>
        <th rowspan="2">Status Sekolah</th>
        <th rowspan="2">Nama Satuan Pendidikan</th>
        <th rowspan="2">Provinsi</th>
        <th rowspan="2">Kota</th>
        <th rowspan="2"></th>
      </tr>
    </thead>
    <tbody>
      @php
      $no = 1
      @endphp
      @foreach($pelajar as $p)
      <tr>
        <th scope="row">{{ $no++ }}</th>
        <td>{{ $p->nik }}</td>
        <td>{{ $p->nama }}</td>
        <td>{{ $p->tempat_lahir }}, {{ $p->tanggal_lahir }}</td>
        <td>{{ $p->kelamin }}</td>
        <td>{{ $p->orang_tua }}</td>
        <td>{{ $p->status_sekolah }}</td>
        <td>{{ $p->sekolah_nama }}</td>
        <td>{{ $p->provinsimodel->provinsi }}</td>
        <td>{{ $p->kotamodel->kota }}</td>
        <td>
          <a class="anjai nav-link text-center dropdown" href="#" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
            <span data-feather="list"></span>
            More
          </a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
            <li>
              <a class="anjai2 nav-link btn-success text-white" href="{{ url('/sekolah/export', $p->id) }}">
                <span class="iconify" data-icon="cil:print"></span>
                Print
              </a>
            </li>
            <li>
            <li>
              <a class="anjai2 nav-link btn-primary text-white" href="{{ url('/edit', $p->id) }}">
                <span class="iconify" data-icon="bytesize:edit"></span>
                Edit
              </a>
            </li>
            <li>
              <a class="anjai2 nav-link btn-danger text-white" href="{{ url('/destroy', $p->id) }}" onclick="return confirm('Are you sure you want to delete it?')">
                <span class="iconify" data-icon="fluent:delete-20-regular"></span>
                Delete
              </a>
            </li>
          </ul>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <br />
  Halaman : {{ $pelajar->currentPage() }} <br />
  Jumlah Data : {{ $pelajar->total() }} <br />
  Data Per Halaman : {{ $pelajar->perPage() }} <br />


  {{ $pelajar->links('pagination::bootstrap-4') }}
</div>

<div id="isi"></div>

</main>
</div>
</div>

<div class="text-light py-3 fixed-bottom bg-dark" style="">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm text-center">
        <div>
          <a class="fs-6 text-decoration-none text-light">Copyright&copy;2022 SMKN 2 Banjarmasin</a>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- <script src="assets/dist/js/bootstrap.bundle.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js" integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js" integrity="sha384-zNy6FEbO50N+Cg5wap8IKA4M/ZnLJgzc6w2NqACZaK0u0FXfOWRRJOnQtpZun8ha" crossorigin="anonymous"></script>
<script src="dashboard.js"></script>
<script src="https://code.iconify.design/2/2.1.2/iconify.min.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript">
  var chart = <?php echo json_encode($chart) ?>;
  Highcharts.chart('isi', {
    title: {
      text: 'Perkembangan Pelajar'
    },
    subtitle: {
      text: 'Tahun 2022'
    },
    xAxis: {
      categories: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
        'October', 'November', 'December'
      ]
    },
    yAxis: {
      title: {
        text: 'Angka kenaikan pelajar'
      }
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle'
    },
    plotOptions: {
      series: {
        allowPointSelect: true
      }
    },
    series: [{
      name: 'Pelajar Baru',
      data: chart
    }],
    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom'
          }
        }
      }]
    }
  });
</script>

@endsection