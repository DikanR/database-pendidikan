<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class statusmodel extends Model
{
    // use HasFactory;

    private static $data_title = [
        'Halaman',
        'Data Siswa',
        'Integrasi'
    ];

    private static $data_icon = [
        'Halaman' => 'home',
        'Data Siswa' => 'file-text',
        'Integrasi' => 'layers'
    ];

    private static $data_link = [
        'Halaman' => '/',
        'Data Siswa' => '/index',
        'Integrasi' => '#'
    ];

    public static function get($query = '')
    {
        if ($query == 'data_title') {
            return self::$data_title;
        } else if ($query == 'data_icon') {
            return self::$data_icon;
        } else if ($query == 'data_link') {
            return self::$data_link;
        }
    }
}
