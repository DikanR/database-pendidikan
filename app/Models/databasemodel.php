<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\provinsimodel;
use App\Models\kotamodel;

class databasemodel extends Model
{
    //use Sortable;

    // use HasFactory;

    protected $table = 'database_pelajar';

    // protected $

    protected $fillable = [
        'nik',
        'nama',
        'tempat_lahir',
        'tanggal_lahir',
        'agama',
        'kelamin',
        'orang_tua',
        'sekolah_nama',
        // 'sekolah_id',
        // 'kota_nama',
        'kotamodel_id',
        // 'provinsi_nama',
        'provinsimodel_id',
        'status_sekolah'
    ];

    public static function CaseKota($str)
    {
        switch ($str) {
            case 'Pontianak':
                return 1;
                break;
            case 'Banjarmasin':
                return 2;
                break;
            case 'Palangkaraya':
                return 3;
                break;
            case 'Samarinda':
                return 4;
                break;
            case 'Tanjung Selor':
                return 5;
                break;
        }
    }

    public static function CaseProvinsi($str)
    {
        switch ($str) {
            case 'Kalimantan Barat':
                return 1;
                break;
            case 'Kalimantan Selatan':
                return 2;
                break;
            case 'Kalimantan Tengah':
                return 3;
                break;
            case 'Kalimantan Timur':
                return 4;
                break;
            case 'Kalimantan Utara':
                return 5;
                break;
        }
    }
    public function provinsimodel() {
        return $this->belongsTo(provinsimodel::class, 'provinsimodel_id', 'id');
    }
    public function kotamodel() {
        return $this->belongsTo(kotamodel::class, 'kotamodel_id', 'id');
    }
    //protected $sortable =['nik', 'nama', 'tempat_lahir', 'tanggal_lahir', 'kelamin', 'orang_tua', 'status_sekolah'];
}
