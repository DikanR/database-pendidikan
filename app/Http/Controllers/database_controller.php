<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\databasemodel;
use App\Models\statusmodel;

use Barryvdh\DomPDF\Facade\Pdf;

class database_controller extends Controller
{
    public function pelajar()
    {
        $chart = databasemodel::select(\DB::raw("COUNT(*) as count"))
            ->whereYear('created_at', date('Y'))
            ->groupBy(\DB::raw("Month(created_at)"))
            ->pluck('count');

        $pelajar = databasemodel::paginate(10);
        $titles = statusmodel::get('data_title');
        $icon = statusmodel::get('data_icon');
        $navlink = statusmodel::get('data_link');
        $active  = $navlink['Data Siswa'];
        // return view('home.datapelajar', compact('chart', 'pelajar'));
        return view('home.datapelajar', compact('chart', 'pelajar', 'titles', 'icon', 'navlink', 'active'));
    }
    public function provinsi()
    {
        return "tot ini provinsi";
    }
    public function kota()
    {
        return "tot ini kota";
    }
    public function sekolah()
    {
        return "tot ini sekolah";
    }

    public function cari(Request $request)
    {
        $chart = databasemodel::select(\DB::raw("COUNT(*) as count"))
            ->whereYear('created_at', date('Y'))
            ->groupBy(\DB::raw("Month(created_at)"))
            ->pluck('count');

        $cari = $request->cari;

        // $pelajar = DB::table('database_pelajar')
        //     ->where('nama', 'like', "%" . $cari . "%")
        //     ->paginate(10);

        $pelajar = databasemodel::where('database_pelajar.nama', 'like', "%" . $cari . "%")->paginate(10);
        $titles = statusmodel::get('data_title');
        $icon = statusmodel::get('data_icon');
        $navlink = statusmodel::get('data_link');
        $active  = $navlink['Data Siswa'];
        // ->join('database_sekolah', 'database_sekolah.id', '=', 'database_pelajar.sekolah_id')
        // ->join('database_kota', 'database_kota.id', '=', 'database_pelajar.id')
        // ->join('database_provinsi', 'database_kota.id', '=', 'database_pelajar.id')

        // ->join('database_kota', 'database_kota.id', '=', 'database_pelajar.kota_id')
        // ->join('database_provinsi', 'database_provinsi.id', '=', 'database_pelajar.provinsi_id')

        // return view('home.datapelajar', ['pelajar' => $pelajar]);
        return view('home.datapelajar', compact('chart', 'pelajar', 'titles', 'icon', 'navlink', 'active'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view("home.inputpelajar");
        return view('home.inputpelajar', [
            'titles' => statusmodel::get('data_title'),
            'icon' => statusmodel::get('data_icon'),
            'navlink' => statusmodel::get('data_link'),
            'active' => statusmodel::get('data_link')['Data Siswa']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nik' => 'required',
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'ttl' => 'required',
            'agama' => 'required',
            'kelamin' => 'required',
            'orang_tua' => 'required',
            'sekolah_nama' => 'required',
            'kota' => 'required',
            'provinsi' => 'required',
            'status_sekolah' => 'required'
        ]);
        $dataSaved = [
            'nik' => $request->nik,
            'nama' => $request->nama,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->ttl,
            'agama' => $request->agama,
            'kelamin' => $request->kelamin,
            'orang_tua' => $request->orang_tua,
            'sekolah_nama' => $request->sekolah_nama,
            'kotamodel_id' => databasemodel::CaseKota($request->kota),
            'provinsimodel_id' => databasemodel::CaseProvinsi($request->provinsi),
            'status_sekolah' => $request->status_sekolah
        ];
        // $aa = $request->all();
        // databasemodel::insert($data);
        databasemodel::create($dataSaved);
        // return redirect('datapelajar')->with('success', 'Data telah ditambahkan.');
        // return view('home.datapelajar');
        return redirect('/index');
    }

    // public function inputpelajar()
    // {
    //     return view("home.inputpelajar");
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tt = databasemodel::where('id', $id)->first();
        $tt->delete();
        return redirect('/index');
    }
    public function edit($id)
    {
        $pelajar = databasemodel::where('id', $id)->first();
        // $titles = statusmodel::get('data_title');
        // $icon = statusmodel::get('data_icon');
        // $navlink = statusmodel::get('data_link');
        // $active  = $navlink['Data Siswa'];
        // return view('home.editpelajar', ['pelajar' => $pelajar]);
        return view('home.editpelajar', [
            'pelajar' => $pelajar,
            'titles' => statusmodel::get('data_title'),
            'icon' => statusmodel::get('data_icon'),
            'navlink' => statusmodel::get('data_link'),
            'active' => statusmodel::get('data_link')['Data Siswa']
        ]);
    }
    public function editsubmit(Request $request, $id)
    {
        $request->validate([
            'nik' => 'required',
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'ttl' => 'required',
            'agama' => 'required',
            'kelamin' => 'required',
            'orang_tua' => 'required',
            'sekolah_nama' => 'required',
            'kota' => 'required',
            'provinsi' => 'required',
            'status_sekolah' => 'required'
        ]);
        $dataSaved = [
            'nik' => $request->nik,
            'nama' => $request->nama,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->ttl,
            'agama' => $request->agama,
            'kelamin' => $request->kelamin,
            'orang_tua' => $request->orang_tua,
            'sekolah_nama' => $request->sekolah_nama,
            'kotamodel_id' => databasemodel::CaseKota($request->kota),
            'provinsimodel_id' => databasemodel::CaseProvinsi($request->provinsi),
            'status_sekolah' => $request->status_sekolah
        ];

        $pelajar = databasemodel::find($id);
        $pelajar->update($dataSaved);

        return redirect('/index');
    }

    public function PDFpelajar($id)
    {
        $pelajarpdf = databasemodel::where('id', $id)->first();

        $pdf = PDF::loadview('home.datapelajarPDF', ['pelajarpdf' => $pelajarpdf]);
        return $pdf->stream('laporan-siswa-pdf');
    }
}
