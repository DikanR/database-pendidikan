<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\authmodel;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class auth_controller extends Controller
{
    public function login(){
        return view('login');
    }

    public function loginsubmit(Request $request){
        $login = $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        if(Auth::attempt($login)){
            $request->session()->regenerate();

            return redirect()->intended('/index');
        }

        return back()->with('LoginError', 'Username atau password salah');
    }

    public function registration(){
        return view('register');
    }

    public function registrationsubmit(Request $request){
        $validate = $request->validate([
            'username' => 'required|unique:user|min:3|max:15',
            'password' => 'required|min:8'
        ]);

        $validate['password'] = bcrypt($validate['password']);

        User::create($validate);
        $request->session()->flash('success', 'Registrasi berhasil');

        return redirect('/');
    }
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
